﻿using System.ComponentModel.DataAnnotations;

namespace ERD_CodeFirst.Models
{
    public class Transactions
    {
        public string Id { get; set; }
        public Employees Emplyee { get; set; }
        public Customer Customer { get; set; }
        [Required, StringLength(100)]
        public string Name { get; set; }
    }
}
