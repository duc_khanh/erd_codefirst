﻿using System.ComponentModel.DataAnnotations;

namespace ERD_CodeFirst.Models
{
    public class Logs
    {
        public string Id { get; set; }
        public Transactions Transactional { get; set; }
        [Required]
        public DateOnly LoginDate { get; set; }
        [Required]
        public TimeOnly LoginTime { get; set; }
    }
}
