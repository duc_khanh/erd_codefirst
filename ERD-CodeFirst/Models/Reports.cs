﻿using System.ComponentModel.DataAnnotations;

namespace ERD_CodeFirst.Models
{
    public class Reports
    {
        public string Id { get; set; }
        public Accounts Account { get; set; }
        public Logs Logs { get; set; }
        public Transactions Transactional { get; set; }
        [Required, StringLength(100)]
        public string ReportName { get; set; }
        [Required]
        public DateTime ReportDate { get; set; }

    }
}
