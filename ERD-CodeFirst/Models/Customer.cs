﻿using System.ComponentModel.DataAnnotations;

namespace ERD_CodeFirst.Models
{
    public class Customer
    {
        public string Id { get; set; }
        [Required, StringLength(100)]
        public string FirstName { get; set; }
        [Required, StringLength(100)]
        public string LastName { get; set; }
        public string Contact { get; set; }
        public string Address { get; set; }
        [Required, StringLength(100)]
        public string UserName { get; set; }
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }





    }
}
