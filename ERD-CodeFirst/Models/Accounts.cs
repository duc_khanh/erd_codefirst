﻿using System.ComponentModel.DataAnnotations;

namespace ERD_CodeFirst.Models
{
    public class Accounts
    {
        public string Id { get; set; }
        public Customer Customer { get; set; }
        [Required, StringLength(100)]
        public string AccountName { get; set; }
    }
}
